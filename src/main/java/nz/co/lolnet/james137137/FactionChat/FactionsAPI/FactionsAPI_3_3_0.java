/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nz.co.lolnet.james137137.FactionChat.FactionsAPI;

import com.massivecraft.factions.Rel;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.MPlayer;
import com.massivecraft.factions.entity.Rank;


/**
 *
 * @author awcjack
 */
public class FactionsAPI_3_3_0 implements FactionsAPI{

    @Override
    public Faction getFaction(Object player){
        MPlayer uplayer = MPlayer.get(player);
        Faction faction = uplayer.getFaction();
        return faction;
    }

    @Override
    public String getFactionName(Object player) {
        MPlayer uplayer = MPlayer.get(player);
        Faction faction = uplayer.getFaction();
        return faction.getName();
    }

    @Override
    public String getFactionID(Object player) {
        MPlayer uplayer = MPlayer.get(player);
        Faction faction = uplayer.getFaction();
        return faction.getUniverse()+"-" + getFactionName(player);
    }

    @Override
    public Rel getRelationship(Object player1, Object player2) {
        MPlayer uplayer1 = MPlayer.get(player1);
        MPlayer uplayer2 = MPlayer.get(player2);

        Rel rel = uplayer1.getRelationTo(uplayer2.getFaction());
        return rel  ;
    }

    @Override
    public boolean isFactionless(Object player) {
        return MPlayer.get(player).getFaction().getName().contains("Wilderness");
    }

    @Override
    public String getPlayerTitle(Object player) {
        String title = MPlayer.get(player).getTitle();
        if (title.contains("no title set")) {
            return "";
        }
        return title;
    }

    @Override
    public Rank getPlayerRank(Object player) {
        Rank role = MPlayer.get(player).getRank();
        return role;
    }
    
}
